def call(Map config = [:])
{
installPackage(name: 'ansible')
runTimeApproval(message: 'want to proceed further?')
gitClone (git: "${config.git}")
ansiblePlaybook inventory: '/var/lib/jenkins/workspace/${JOB_NAME}/ansiblerole/inventory', playbook: '/var/lib/jenkins/workspace/${JOB_NAME}/ansiblerole/ossec.yml
}
